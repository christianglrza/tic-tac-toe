import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {
  newGame:boolean=false
  casillas:string[]
  sigJugador:boolean
  ganador:string

  constructor(private dialog:MatDialog) { }

  ngOnInit() {
    this.reiniciarJuego()
  }

  reiniciarJuego(){
    this.casillas = Array(9).fill(null)
    this.ganador = ''
    this.sigJugador = true
  }

  iniciar(){
    this.newGame = !this.newGame
  }

  get jugador(){
    return this.sigJugador ? 'X' : 'O'
  }

  movimiento(idx: number) {
    if (!this.casillas[idx]) {
      this.casillas.splice(idx, 1, this.jugador);
      this.sigJugador = !this.sigJugador;
    }
    this.ganador = this.calculateWinner();
    if(this.ganador){
      this.dialog.open(DialogComponent,{disableClose:true,data:{ganador: this.ganador}})
      .afterClosed().subscribe(data=>{
        if(data===1){
          this.reiniciarJuego()
        }else{
          this.reiniciarJuego()
          this.iniciar()
        }
      })
    }
    if(this.casillas.every(element => element != null)){
      this.reiniciarJuego()
    }
    
  }

  calculateWinner() {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        this.casillas[a] &&
        this.casillas[a] === this.casillas[b] &&
        this.casillas[a] === this.casillas[c]
      ) {
        return this.casillas[a];
      }
    }
    return null;
  }
}
