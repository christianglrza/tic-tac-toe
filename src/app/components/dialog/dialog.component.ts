import { Component, OnInit, Inject} from '@angular/core';
import { MAT_DIALOG_DATA,MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data:any, private dialogRef:MatDialogRef<DialogComponent>) { }

  ngOnInit() {
  }

  iniciar(){
    this.dialogRef.close(1)
  }
  terminar(){
    this.dialogRef.close(2)
  }
}
