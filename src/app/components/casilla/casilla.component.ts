import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-casilla',
  templateUrl: './casilla.component.html',
  styleUrls: ['./casilla.component.css']
})
export class CasillaComponent implements OnInit {
  @Input() value: 'X' | 'O'

  constructor() { }

  ngOnInit() {
  }

}
